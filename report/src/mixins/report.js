var reportMixin = {
	data () {
			return {
				
			}
	},
	methods: {
		itemIsDisabled(type, value){
			if(this.report.columnList.findIndex( reportItem => reportItem.type == type && reportItem.value == value ) >= 0){
				return true;
			}
			switch(this.report.baseModule){
				case 'spouse_registration':
					switch(type){
						case 'workshop':
						case 'workshop_custom_form':
						case 'merchandise':
						case 'merchandise_custom_form':
						case 'tour':
						case 'tour_custom_form':
						case 'hotel':
						case 'hotel_custom_form':
						case 'donation':
							return true;
							break;
					}
					break;
				case 'workshop':
					switch(type){
						case 'spouse_registration':
						case 'spouse_registration_custom_form':
						case 'merchandise':
						case 'merchandise_custom_form':
						case 'tour':
						case 'tour_custom_form':
						case 'hotel':
						case 'hotel_custom_form':
						case 'donation':
							return true;
							break;
					}
					break;
				case 'merchandise':
					switch(type){
						case 'spouse_registration':
						case 'spouse_registration_custom_form':
						case 'workshop':
						case 'workshop_custom_form':
						case 'tour':
						case 'tour_custom_form':
						case 'hotel':
						case 'hotel_custom_form':
						case 'donation':
							return true;
							break;
					}
					break;
				case 'tour':
					switch(type){
						case 'spouse_registration':
						case 'spouse_registration_custom_form':
						case 'workshop':
						case 'workshop_custom_form':
						case 'merchandise':
						case 'merchandise_custom_form':
						case 'hotel':
						case 'hotel_custom_form':
						case 'donation':
							return true;
							break;
					}
					break;
				case 'hotel':
					switch(type){
						case 'spouse_registration':
						case 'spouse_registration_custom_form':
						case 'workshop':
						case 'workshop_custom_form':
						case 'merchandise':
						case 'merchandise_custom_form':
						case 'tour':
						case 'tour_custom_form':
						case 'donation':
							return true;
							break;
					}
					break;
				case 'donation':
					switch(type){
						case 'spouse_registration':
						case 'spouse_registration_custom_form':
						case 'workshop':
						case 'workshop_custom_form':
						case 'merchandise':
						case 'merchandise_custom_form':
						case 'tour':
						case 'tour_custom_form':
						case 'hotel':
						case 'hotel_custom_form':
							return true;
							break;
					}
					break;
			}
			return false;
		},
	}
}

export default reportMixin
export const COLUMN_LIST = {
	'registration': [
		{
			'title': 'Fullname',
			'value': 'fullname',
			'type': 'registration'
		},
		{
			'title': 'Firstname',
			'value': 'firstname',
			'type': 'registration'
		},
		{
			'title': 'Lastname',
			'value': 'lastname',
			'type': 'registration'
		},
		{
			'title': 'Image',
			'value': 'image',
			'type': 'registration'
		},
		{
			'title': 'E-mail',
			'value': 'email',
			'type': 'registration'
		},
		{
			'title': 'Registration Type',
			'value': 'registration_type_title',
			'type': 'registration'
		},
		{
			'title': 'Registration Period',
			'value': 'registration_period_title',
			'type': 'registration'
		},
		{
			'title': 'Payment Status',
			'value': 'payment_status',
			'type': 'registration'
		},
		{
			'title': 'Payment Amount',
			'value': 'payment_amount',
			'type': 'registration'
		}
	],
	'participant_custom_form': [
		{
			'title': 'Address',
			'value': 'custom_form_1', //unique custom form field id
			'type': 'participant_custom_form'
		},
		{
			'title': 'Gender',
			'value': 'custom_form_2', //unique custom form field id
			'type': 'participant_custom_form'
		},
		{
			'title': 'Country',
			'value': 'custom_form_3', //unique custom form field id
			'type': 'participant_custom_form'
		}
	],
	'registration_type_custom_form': [
		{
			'registration_type_id': '1',
			'registration_type_title': 'Student',
			'custom_form': [ 
				{ 
					'title': 'School',
					'value': 'custom_form_4',
					'type': 'registration_type_custom_form'
				},
				{ 
					'title': 'Student Number',
					'value': 'custom_form_5',
					'type': 'registration_type_custom_form'
				}
			]
		},
		{
			'registration_type_id': '2',
			'registration_type_title': 'Regular',
			'custom_form': [ 
				{ 
					'title': 'Company/Institute',
					'value': 'custom_form_6',
					'type': 'registration_type_custom_form'
				}
			]
		}
	],
	'spouse_registration': [
		{
			'title': 'Spouse Fullname',
			'value': 'spouse_fullname',
			'type': 'spouse_registration'
		},
		{
			'title': 'Spouse Firstname',
			'value': 'spouse_firstname',
			'type': 'spouse_registration'
		},
		{
			'title': 'Spouse Lastname',
			'value': 'spouse_lastname',
			'type': 'spouse_registration'
		},
		{
			'title': 'Payment Amount',
			'value': 'spouse_payment_amount',
			'type': 'spouse_registration'
		},
		{
			'title': 'Payment Status',
			'value': 'spouse_payment_status',
			'type': 'spouse_registration'
		}
	],
	'spouse_registration_custom_form': [
		{ 
			'title': 'Job',
			'value': 'custom_form_7',
			'type': 'spouse_registration_custom_form',
			'custom_form_fields': [
				{ 
					'title': 'Job City',
					'value': 'custom_form_17',
					'type': 'spouse_registration_custom_form',
					'custom_form_fields': [
						{ 
							'title': 'Job City 2',
							'value': 'custom_form_18',
							'type': 'spouse_registration_custom_form',
							'custom_form_fields': [
								{ 
									'title': 'Job City 3',
									'value': 'custom_form_20',
									'type': 'spouse_registration_custom_form',
								},
							]
						},
					]
				},
				{ 
					'title': 'Job 2',
					'value': 'custom_form_21',
					'type': 'spouse_registration_custom_form',
				}
			]
		},
		{ 
			'title': 'Gender',
			'value': 'custom_form_8',
			'type': 'spouse_registration_custom_form',
			'custom_form_fields': [
				{ 
					'title': 'Gender 2 ',
					'value': 'custom_form_19',
					'type': 'spouse_registration_custom_form',
				},
			]
		}
	],
	'workshop': [
		{
			'title': 'Workshop Title',
			'value': 'workshop_title',
			'type': 'workshop'
		},
		{
			'title': 'Workshop Payment Status',
			'value': 'workshop_payment_status',
			'type': 'workshop'
		},
		{
			'title': 'Workshop Payment Amount',
			'value': 'workshop_payment_amount',
			'type': 'workshop'
		}
	],
	'workshop_custom_form': [
		{ 
			'title': 'Workshop Type',
			'value': 'custom_form_9',
			'type': 'workshop_custom_form'
		},
		{ 
			'title': 'Workshop Time',
			'value': 'custom_form_10',
			'type': 'workshop_custom_form'
		}
	],
	'merchandise': [
		{
			'title': 'Merchandise Title',
			'value': 'merchandise_title',
			'type': 'merchandise'
		},
		{
			'title': 'Merchandise Payment Status',
			'value': 'merchandise_payment_status',
			'type': 'merchandise'
		},
		{
			'title': 'Merchandise Payment Amount',
			'value': 'merchandise_payment_amount',
			'type': 'merchandise'
		}
	],
	'merchandise_custom_form': [
		{ 
			'title': 'Size',
			'value': 'custom_form_11',
			'type': 'merchandise_custom_form'
		},
		{ 
			'title': 'Color',
			'value': 'custom_form_12',
			'type': 'merchandise_custom_form'
		}
	],
	'tour': [
		{
			'title': 'Tour Title',
			'value': 'tour_title',
			'type': 'tour'
		},
		{
			'title': 'Tour Payment Status',
			'value': 'tour_payment_status',
			'type': 'tour'
		},
		{
			'title': 'Tour Payment Amount',
			'value': 'tour_payment_amount',
			'type': 'tour'
		}
	],
	'tour_custom_form': [
		{ 
			'title': 'Day',
			'value': 'custom_form_13',
			'type': 'tour_custom_form'
		},
		{ 
			'title': 'Time',
			'value': 'custom_form_14',
			'type': 'tour_custom_form'
		}
	],
	'hotel': [
		{
			'title': 'Hotel Title',
			'value': 'hotel_title',
			'type': 'hotel'
		},
		{
			'title': 'Room Type Title',
			'value': 'hotel_room_type_title',
			'type': 'hotel'
		},
		{
			'title': 'Hotel Payment Status',
			'value': 'hotel_payment_status',
			'type': 'hotel'
		},
		{
			'title': 'Hotel Payment Amount',
			'value': 'hotel_payment_amount',
			'type': 'hotel'
		},
		{
			'title': 'Hotel Check-in Date',
			'value': 'hotel_check_in_date',
			'type': 'hotel'
		},
		{
			'title': 'Hotel Check-out Date',
			'value': 'hotel_check_out_date',
			'type': 'hotel'
		}
	],
	'hotel_custom_form': [
		{ 
			'title': 'Kids',
			'value': 'custom_form_15',
			'type': 'hotel_custom_form'
		},
		{ 
			'title': 'Pet',
			'value': 'custom_form_16',
			'type': 'hotel_custom_form'
		}
	],
	'donation': [
		{
			'title': 'Donation Payment Status',
			'value': 'donation_payment_status',
			'type': 'donation'
		},
		{
			'title': 'Donation Payment Amount',
			'value': 'donation_payment_amount',
			'type': 'donation'
		}
	],
	'payment': [
		{
			'title': 'Payment Services',
			'value': 'payment_services',
			'type': 'payment'
		},
		{
			'title': 'Payment Status',
			'value': 'payment_status',
			'type': 'payment'
		},
		{
			'title': 'Payment Amount',
			'value': 'payment_amount',
			'type': 'payment'
		},
		{
			'title': 'Payment Balance Amount',
			'value': 'payment_balance_amount',
			'type': 'payment'
		},
		{
			'title': 'Payment Discount Amount',
			'value': 'payment_discount_amount',
			'type': 'payment'
		},
		{
			'title': 'Payment Penalty Amount',
			'value': 'payment_penalty_amount',
			'type': 'payment'
		},
		{
			'title': 'Payment Refund Amount',
			'value': 'payment_refund_amount',
			'type': 'payment'
		},
		{
			'title': 'Payment Idle Amount',
			'value': 'payment_idle_amount',
			'type': 'payment'
		},
		{
			'title': 'Payment Date',
			'value': 'payment_date',
			'type': 'payment'
		},
		{
			'title': 'Payment Type',
			'value': 'payment_type',
			'type': 'payment'
		}
	]

};

export const REGISTRATION_LIST = [
	{
		"id": 0,
		"firstname": "Jimmie",
		"lastname": "Maxwell",
		"email": "jimmiemaxwell@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "411 Drew Street, Balm, California, 6104",
			"custom_form_2": "male",
			"custom_form_3": "Turkey"
		},
		"registration_type_id": 1,
		"registration_type_title": "Student",
		"registration_period_id": 3,
		"registration_period_title": "Late",
		"registration_type_custom_form": {
			"custom_form_4": "University of Cambridge",
			"custom_form_5": "100122042",
		},
		"payment_status": true,
		"payment_amount": "$187.57"
	},
	{
		"id": 1,
		"firstname": "Shannon",
		"lastname": "Crawford",
		"email": "shannoncrawford@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "974 Quentin Road, Martell, Hawaii, 820",
			"custom_form_2": "male",
			"custom_form_3": "Spain"
		},
		"registration_type_id": 2,
		"registration_type_title": "Regular",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {
			"custom_form_6": "MeetingHand"
		},
		"payment_status": false,
		"payment_amount": "$123.09"
	},
	{
		"id": 2,
		"firstname": "Ayala",
		"lastname": "Lester",
		"email": "ayalalester@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "408 Christopher Avenue, Longoria, Illinois, 7559",
			"custom_form_2": "female",
			"custom_form_3": "Greece"
		},
		"registration_type_id": 2,
		"registration_type_title": "Regular",
		"registration_period_id": 2,
		"registration_period_title": "Normal",
		"registration_type_custom_form": {
			"custom_form_6": "MeetingHand"
		},
		"payment_status": true,
		"payment_amount": "$209.00"
	},
	{
		"id": 3,
		"firstname": "Hines",
		"lastname": "Langley",
		"email": "hineslangley@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "940 Furman Street, Warsaw, Colorado, 4894",
			"custom_form_2": "female",
			"custom_form_3": "Greece"
		},
		"registration_type_id": 3,
		"registration_type_title": "Attendee",
		"registration_period_id": 2,
		"registration_period_title": "Normal",
		"registration_type_custom_form": {},
		"payment_status": true,
		"payment_amount": "$241.69"
	},
	{
		"id": 4,
		"firstname": "Jordan",
		"lastname": "Delgado",
		"email": "jordandelgado@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "999 Oxford Street, Cetronia, New Jersey, 7482",
			"custom_form_2": "male",
			"custom_form_3": "England"
		},
		"registration_type_id": 3,
		"registration_type_title": "Attendee",
		"registration_period_id": 2,
		"registration_period_title": "Normal",
		"registration_type_custom_form": {},
		"payment_status": false,
		"payment_amount": "$159.13"
	},
	{
		"id": 5,
		"firstname": "Booker",
		"lastname": "Baird",
		"email": "bookerbaird@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "754 Madoc Avenue, Robbins, North Dakota, 4211",
			"custom_form_2": "male",
			"custom_form_3": "Greece"
		},
		"registration_type_id": 1,
		"registration_type_title": "Student",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {
			"custom_form_4": "University of Cambridge",
			"custom_form_5": "100122042",
		},
		"payment_status": true,
		"payment_amount": "$194.70"
	},
	{
		"id": 6,
		"firstname": "Bette",
		"lastname": "Byrd",
		"email": "bettebyrd@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "686 Essex Street, Jardine, Ohio, 5214",
			"custom_form_2": "female",
			"custom_form_3": "Greece"
		},
		"registration_type_id": 1,
		"registration_type_title": "Student",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {
			"custom_form_4": "University of Cambridge",
			"custom_form_5": "100122042",
		},
		"payment_status": true,
		"payment_amount": "$199.79"
	},
	{
		"id": 7,
		"firstname": "Vickie",
		"lastname": "Orr",
		"email": "vickieorr@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "473 Channel Avenue, Cresaptown, Maine, 1636",
			"custom_form_2": "male",
			"custom_form_3": "England"
		},
		"registration_type_id": 1,
		"registration_type_title": "Student",
		"registration_period_id": 3,
		"registration_period_title": "Late",
		"registration_type_custom_form": {
			"custom_form_4": "University of Cambridge",
			"custom_form_5": "100122042",
		},
		"payment_status": true,
		"payment_amount": "$263.98"
	},
	{
		"id": 8,
		"firstname": "Ursula",
		"lastname": "Rivas",
		"email": "ursularivas@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "605 Lenox Road, Ezel, Northern Mariana Islands, 1732",
			"custom_form_2": "male",
			"custom_form_3": "Greece"
		},
		"registration_type_id": 1,
		"registration_type_title": "Student",
		"registration_period_id": 3,
		"registration_period_title": "Late",
		"registration_type_custom_form": {
			"custom_form_4": "University of Cambridge",
			"custom_form_5": "100122042",
		},
		"payment_status": false,
		"payment_amount": "$211.34"
	},
	{
		"id": 9,
		"firstname": "Dominguez",
		"lastname": "Marquez",
		"email": "dominguezmarquez@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "976 Hampton Place, Calpine, Federated States Of Micronesia, 2343",
			"custom_form_2": "female",
			"custom_form_3": "England"
		},
		"registration_type_id": 2,
		"registration_type_title": "Regular",
		"registration_period_id": 2,
		"registration_period_title": "Normal",
		"registration_type_custom_form": {
			"custom_form_6": "Twitter"
		},
		"payment_status": true,
		"payment_amount": "$35.70"
	},
	{
		"id": 10,
		"firstname": "Beth",
		"lastname": "Vaughan",
		"email": "bethvaughan@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "438 Aberdeen Street, Winesburg, American Samoa, 3034",
			"custom_form_2": "male",
			"custom_form_3": "Spain"
		},
		"registration_type_id": 2,
		"registration_type_title": "Regular",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {
			"custom_form_6": "LinkedIn"
		},
		"payment_status": true,
		"payment_amount": "$17.40"
	},
	{
		"id": 11,
		"firstname": "Mosley",
		"lastname": "English",
		"email": "mosleyenglish@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "677 India Street, Carlos, Virginia, 9828",
			"custom_form_2": "male",
			"custom_form_3": "Spain"
		},
		"registration_type_id": 1,
		"registration_type_title": "Student",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {
			"custom_form_4": "University of Cambridge",
			"custom_form_5": "100122042",
		},
		"payment_status": false,
		"payment_amount": "$179.49"
	},
	{
		"id": 12,
		"firstname": "Lou",
		"lastname": "Garrett",
		"email": "lougarrett@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "496 Girard Street, Fresno, Montana, 8319",
			"custom_form_2": "male",
			"custom_form_3": "Spain"
		},
		"registration_type_id": 3,
		"registration_type_title": "Attendee",
		"registration_period_id": 2,
		"registration_period_title": "Normal",
		"registration_type_custom_form": {},
		"payment_status": false,
		"payment_amount": "$120.47"
	},
	{
		"id": 13,
		"firstname": "Deanne",
		"lastname": "Haley",
		"email": "deannehaley@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "894 Harden Street, Bowmansville, Connecticut, 7184",
			"custom_form_2": "male",
			"custom_form_3": "Turkey"
		},
		"registration_type_id": 3,
		"registration_type_title": "Attendee",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {},
		"payment_status": true,
		"payment_amount": "$45.71"
	},
	{
		"id": 14,
		"firstname": "Jessica",
		"lastname": "Sweet",
		"email": "jessicasweet@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "436 Emmons Avenue, Eastvale, Oklahoma, 6374",
			"custom_form_2": "male",
			"custom_form_3": "Greece"
		},
		"registration_type_id": 2,
		"registration_type_title": "Regular",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {
			"custom_form_6": "Facebook"
		},
		"payment_status": false,
		"payment_amount": "$228.16"
	},
	{
		"id": 15,
		"firstname": "Hobbs",
		"lastname": "Rutledge",
		"email": "hobbsrutledge@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "492 Roosevelt Place, Linwood, North Carolina, 867",
			"custom_form_2": "male",
			"custom_form_3": "Greece"
		},
		"registration_type_id": 3,
		"registration_type_title": "Attendee",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {},
		"payment_status": false,
		"payment_amount": "$25.47"
	},
	{
		"id": 16,
		"firstname": "Hyde",
		"lastname": "Hawkins",
		"email": "hydehawkins@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "814 Union Avenue, Fedora, Arizona, 8373",
			"custom_form_2": "male",
			"custom_form_3": "Spain"
		},
		"registration_type_id": 1,
		"registration_type_title": "Student",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {
			"custom_form_4": "University of Oxford",
			"custom_form_5": "100122042",
		},
		"payment_status": false,
		"payment_amount": "$21.04"
	},
	{
		"id": 17,
		"firstname": "Adela",
		"lastname": "Hebert",
		"email": "adelahebert@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "796 Alabama Avenue, Glenville, Massachusetts, 4561",
			"custom_form_2": "male",
			"custom_form_3": "Turkey"
		},
		"registration_type_id": 2,
		"registration_type_title": "Regular",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {
			"custom_form_6": "Cvent"
		},
		"payment_status": false,
		"payment_amount": "$67.00"
	},
	{
		"id": 18,
		"firstname": "Twila",
		"lastname": "Henry",
		"email": "twilahenry@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "837 Dekalb Avenue, Nord, New York, 8740",
			"custom_form_2": "male",
			"custom_form_3": "England"
		},
		"registration_type_id": 1,
		"registration_type_title": "Student",
		"registration_period_id": 2,
		"registration_period_title": "Normal",
		"registration_type_custom_form": {
			"custom_form_4": "University of Oxford",
			"custom_form_5": "100122042",
		},
		"payment_status": true,
		"payment_amount": "$165.90"
	},
	{
		"id": 19,
		"firstname": "Penelope",
		"lastname": "Lynch",
		"email": "penelopelynch@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "117 Homecrest Court, Marenisco, Puerto Rico, 9916",
			"custom_form_2": "male",
			"custom_form_3": "Spain"
		},
		"registration_type_id": 1,
		"registration_type_title": "Student",
		"registration_period_id": 3,
		"registration_period_title": "Late",
		"registration_type_custom_form": {
			"custom_form_4": "University of Oxford",
			"custom_form_5": "100122042",
		},
		"payment_status": true,
		"payment_amount": "$167.48"
	},
	{
		"id": 20,
		"firstname": "Oneil",
		"lastname": "Barrett",
		"email": "oneilbarrett@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "602 Interborough Parkway, Bowden, Michigan, 6270",
			"custom_form_2": "male",
			"custom_form_3": "Spain"
		},
		"registration_type_id": 2,
		"registration_type_title": "Regular",
		"registration_period_id": 3,
		"registration_period_title": "Late",
		"registration_type_custom_form": {
			"custom_form_6": "Etouches"
		},
		"payment_status": true,
		"payment_amount": "$288.00"
	},
	{
		"id": 21,
		"firstname": "Booth",
		"lastname": "Thornton",
		"email": "booththornton@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "930 Pioneer Street, Martell, Indiana, 4526",
			"custom_form_2": "female",
			"custom_form_3": "Spain"
		},
		"registration_type_id": 1,
		"registration_type_title": "Student",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {
			"custom_form_4": "University of Cambridge",
			"custom_form_5": "100122042",
		},
		"payment_status": false,
		"payment_amount": "$280.65"
	},
	{
		"id": 22,
		"firstname": "Cohen",
		"lastname": "Patton",
		"email": "cohenpatton@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "187 Holmes Lane, Ruffin, Guam, 7017",
			"custom_form_2": "male",
			"custom_form_3": "Turkey"
		},
		"registration_type_id": 1,
		"registration_type_title": "Student",
		"registration_period_id": 2,
		"registration_period_title": "Normal",
		"registration_type_custom_form": {
			"custom_form_4": "University of Cambridge",
			"custom_form_5": "100122042",
		},
		"payment_status": false,
		"payment_amount": "$274.26"
	},
	{
		"id": 23,
		"firstname": "Jocelyn",
		"lastname": "Robinson",
		"email": "jocelynrobinson@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "318 Delmonico Place, Wescosville, Florida, 7212",
			"custom_form_2": "female",
			"custom_form_3": "Greece"
		},
		"registration_type_id": 2,
		"registration_type_title": "Regular",
		"registration_period_id": 2,
		"registration_period_title": "Normal",
		"registration_type_custom_form": {
			"custom_form_6": "MeetingHand"
		},
		"payment_status": false,
		"payment_amount": "$243.18"
	},
	{
		"id": 24,
		"firstname": "Blanche",
		"lastname": "Simpson",
		"email": "blanchesimpson@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "569 Highland Avenue, Beechmont, Minnesota, 1295",
			"custom_form_2": "female",
			"custom_form_3": "Turkey"
		},
		"registration_type_id": 1,
		"registration_type_title": "Student",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {
			"custom_form_4": "University of Cambridge",
			"custom_form_5": "100122042",
		},
		"payment_status": false,
		"payment_amount": "$188.82"
	},
	{
		"id": 25,
		"firstname": "Lori",
		"lastname": "Golden",
		"email": "lorigolden@combot.com",
		"image": "http://placehold.it/128x128",
		"custom_form": {
			"custom_form_1": "487 Olive Street, Richville, Delaware, 6907",
			"custom_form_2": "female",
			"custom_form_3": "Turkey"
		},
		"registration_type_id": 2,
		"registration_type_title": "Regular",
		"registration_period_id": 1,
		"registration_period_title": "Early",
		"registration_type_custom_form": {
			"custom_form_6": "MeetingHand"
		},
		"payment_status": true,
		"payment_amount": "$151.00"
	}
];

export const SPOUSE_REGISTRATION_LIST = [
	{
		"id": 0,
		"registration_id": 0,
		"spouse_firstname": "Bruce",
		"spouse_lastname": "Hurst",
		"spouse_payment_status": true,
		"spouse_payment_amount": "$216.52",
		"spouse_custom_form": []
	},
	{
		"id": 1,
		"registration_id": 1,
		"spouse_firstname": "Misty",
		"spouse_lastname": "Kim",
		"spouse_payment_status": true,
		"spouse_payment_amount": "$115.15",
		"spouse_custom_form": {
			"custom_form_7": "Architect",
			"custom_form_20": "deneme",
			"custom_form_8": "Male"
		}
	},
	{
		"id": 2,
		"registration_id": 2,
		"spouse_firstname": "Marci",
		"spouse_lastname": "Garcia",
		"spouse_payment_status": false,
		"spouse_payment_amount": "$84.63",
		"spouse_custom_form": []
	},
	{
		"id": 3,
		"registration_id": 3,
		"spouse_firstname": "Holly",
		"spouse_lastname": "Orr",
		"spouse_payment_status": false,
		"spouse_payment_amount": "$297.03",
		"spouse_custom_form": []
	},
	{
		"id": 4,
		"registration_id": 4,
		"spouse_firstname": "Bullock",
		"spouse_lastname": "Pennington",
		"spouse_payment_status": true,
		"spouse_payment_amount": "$174.17",
		"spouse_custom_form": []
	},
	{
		"id": 5,
		"registration_id": 5,
		"spouse_firstname": "Leanne",
		"spouse_lastname": "Brennan",
		"spouse_payment_status": false,
		"spouse_payment_amount": "$61.91",
		"spouse_custom_form": {
			"custom_form_7": "Doctor",
			"custom_form_8": "Male"
		}
	},
	 {
		"id": 6,
		"registration_id": 0,
		"spouse_firstname": "Sawyer",
		"spouse_lastname": "Rivera",
		"spouse_payment_status": true,
		"spouse_payment_amount": "$47.41",
		"spouse_custom_form": {
			"custom_form_7": "Architect",
			"custom_form_8": "Female"
		}
	},
	{
		"id": 7,
		"registration_id": 15,
		"spouse_firstname": "Bennett",
		"spouse_lastname": "Richmond",
		"spouse_payment_status": true,
		"spouse_payment_amount": "$41.90",
		"spouse_custom_form": {
			"custom_form_7": "Housewife",
			"custom_form_8": "Female"
		}
	},
	{
		"id": 8,
		"registration_id": 19,
		"spouse_firstname": "Darlene",
		"spouse_lastname": "Wise",
		"spouse_payment_status": true,
		"spouse_payment_amount": "$151.43",
		"spouse_custom_form": {
			"custom_form_7": "Housewife",
			"custom_form_8": "Male"
		}
	},
	{
		"id": 9,
		"registration_id": 7,
		"spouse_firstname": "Meagan",
		"spouse_lastname": "Frye",
		"spouse_payment_status": true,
		"spouse_payment_amount": "$66.23",
		"spouse_custom_form": []
	},
	{
		"id": 10,
		"registration_id": 20,
		"spouse_firstname": "Rachael",
		"spouse_lastname": "Sears",
		"spouse_payment_status": true,
		"spouse_payment_amount": "$271.98",
		"spouse_custom_form": {
			"custom_form_7": "Doctor",
			"custom_form_8": "Female"
		}
	},
	{
		"id": 11,
		"registration_id": 15,
		"spouse_firstname": "Cindy",
		"spouse_lastname": "Wiley",
		"spouse_payment_status": false,
		"spouse_payment_amount": "$47.83",
		"spouse_custom_form": []
	},
	{
		"id": 12,
		"registration_id": 17,
		"spouse_firstname": "Deborah",
		"spouse_lastname": "Strong",
		"spouse_payment_status": false,
		"spouse_payment_amount": "$67.29",
		"spouse_custom_form": []
	},
	{
		"id": 13,
		"registration_id": 9,
		"spouse_firstname": "Bowman",
		"spouse_lastname": "Combs",
		"spouse_payment_status": false,
		"spouse_payment_amount": "$177.02",
		"spouse_custom_form": []
	},
	{
		"id": 14,
		"registration_id": 8,
		"spouse_firstname": "Dixon",
		"spouse_lastname": "Molina",
		"spouse_payment_status": true,
		"spouse_payment_amount": "$169.70",
		"spouse_custom_form": {
			"custom_form_7": "Teacher",
			"custom_form_8": "Female"
		}
	},
	{
		"id": 15,
		"registration_id": 16,
		"spouse_firstname": "Cabrera",
		"spouse_lastname": "Mullins",
		"spouse_payment_status": true,
		"spouse_payment_amount": "$274.87",
		"spouse_custom_form": []
	}
];

export const WORKSHOP_RESERVATION_LIST = [
	{
		"id": "5ab0c203223db22d6b7c5992",
		"registration_id": 0,
		"payment_id": "5ab4b8b5417ec6e0e6cae9b5",
		"workshop_title": "aute aliqua excepteur",
		"workshop_custom_form": {
			"custom_form_9": "WS C",
			"custom_form_10": "Evening"
		},
		"workshop_payment_status": true,
		"workshop_payment_amount": "$42.23"
	},
	{
		"id": "5ab0c20311753aaf53b0ec97",
		"registration_id": 9,
		"workshop_title": "et officia adipisicing",
		"workshop_custom_form": {
			 "custom_form_9": "WS A",
			"custom_form_10": "Morning"
		},
		"workshop_payment_status": false,
		"workshop_payment_amount": "$200.59"
	},
	{
		"id": "5ab0c2038e0b57600d9f4f75",
		"registration_id": 6,
		"workshop_title": "aute ea sit",
		"workshop_custom_form": {
			"custom_form_9": "WS B",
			"custom_form_10": "Afternoon"
		},
		"workshop_payment_status": false,
		"workshop_payment_amount": "$162.24"
	},
	{
		"id": "5ab0c2030e44a79e8de26f51",
		"registration_id": 9,
		"workshop_title": "aute ea sit",
		"workshop_custom_form": {
			 "custom_form_9": "WS A",
			"custom_form_10": "Morning"
		},
		"workshop_payment_status": false,
		"workshop_payment_amount": "$236.05"
	},
	{
		"id": "5ab0c203a405e96b800f1127",
		"registration_id": 10,
		"workshop_title": "aute aliqua excepteur",
		"workshop_custom_form": {
			 "custom_form_9": "WS A",
			"custom_form_10": "Morning"
		},
		"workshop_payment_status": false,
		"workshop_payment_amount": "$181.53"
	},
	{
		"id": "5ab0c2035e5910c5ae8a4e46",
		"registration_id": 15,
		"workshop_title": "aute ea sit",
		"workshop_custom_form": {
			"custom_form_9": "WS C",
			"custom_form_10": "Afternoon"
		},
		"workshop_payment_status": false,
		"workshop_payment_amount": "$86.25"
	},
	{
		"id": "5ab0c203da0902c63e809f49",
		"registration_id": 14,
		"workshop_title": "aute aliqua excepteur",
		"workshop_custom_form": {
			"custom_form_9": "WS C",
			"custom_form_10": "Morning"
		},
		"workshop_payment_status": true,
		"workshop_payment_amount": "$16.93"
	},
	{
		"id": "5ab0c20362047c09583e2531",
		"registration_id": 8,
		"workshop_title": "aute aliqua excepteur",
		"workshop_custom_form": {},
		"workshop_payment_status": false,
		"workshop_payment_amount": "$297.16"
	},
	{
		"id": "5ab0c2035b80379d2fc3e48b",
		"registration_id": 4,
		"workshop_title": "et officia adipisicing",
		"workshop_custom_form": {},
		"workshop_payment_status": false,
		"workshop_payment_amount": "$297.59"
	},
	{
		"id": "5ab0c20385b1e1bae60244aa",
		"registration_id": 8,
		"workshop_title": "aute aliqua excepteur",
		"workshop_custom_form": {},
		"workshop_payment_status": true,
		"workshop_payment_amount": "$252.29"
	},
	{
		"id": "5ab0c2037dbdd4dc070c0b3e",
		"registration_id": 3,
		"workshop_title": "aute ea sit",
		"workshop_custom_form": {},
		"workshop_payment_status": true,
		"workshop_payment_amount": "$234.32"
	},
	{
		"id": "5ab0c203e3ecb0d324c8cccd",
		"registration_id": 15,
		"workshop_title": "aute aliqua excepteur",
		"workshop_custom_form": {},
		"workshop_payment_status": true,
		"workshop_payment_amount": "$20.11"
	},
	{
		"id": "5ab0c20326fa759be8c90ec9",
		"registration_id": 8,
		"workshop_title": "et officia adipisicing",
		"workshop_custom_form": {},
		"workshop_payment_status": true,
		"workshop_payment_amount": "$35.68"
	},
	{
		"id": "5ab0c2036b792ca0033e7c84",
		"registration_id": 13,
		"workshop_title": "et officia adipisicing",
		"workshop_custom_form": {},
		"workshop_payment_status": true,
		"workshop_payment_amount": "$143.66"
	},
	{
		"id": "5ab0c203d7e7054ae958ed83",
		"registration_id": 14,
		"workshop_title": "aute ea sit",
		"workshop_custom_form": {},
		"workshop_payment_status": true,
		"workshop_payment_amount": "$60.40"
	}
];

export const MERCHANDISE_LIST = [
	{
		"id": "5ab0f60cbe62b86e546c12e5",
		"registration_id": 4,
		"merchandise_title": "T-shirt",
		"merchandise_custom_form": {
			"custom_form_11": "Medium",
			"custom_form_12": "Red"
		},
		"merchandise_payment_status": true,
		"merchandise_payment_amount": "$136.56"
	},
	{
		"id": "5ab0f60cbcec3013481652e8",
		"registration_id": 22,
		"merchandise_title": "T-shirt",
		"merchandise_custom_form": {
			"custom_form_11": "Large",
			"custom_form_12": "Black"
		},
		"merchandise_payment_status": false,
		"merchandise_payment_amount": "$201.53"
	},
	{
		"id": "5ab0f60cea372337c1e8a1ab",
		"registration_id": 6,
		"merchandise_title": "T-shirt",
		"merchandise_custom_form": {
			"custom_form_11": "Large",
			"custom_form_12": "Black"
		},
		"merchandise_payment_status": true,
		"merchandise_payment_amount": "$144.70"
	},
	{
		"id": "5ab0f60c3d794b7715b76904",
		"registration_id": 1,
		"merchandise_title": "T-shirt",
		"merchandise_custom_form": {
			"custom_form_11": "Large",
			"custom_form_12": "Blue"
		},
		"merchandise_payment_status": false,
		"merchandise_payment_amount": "$20.92"
	},
	{
		"id": "5ab0f60c3519497754423edc",
		"registration_id": 2,
		"merchandise_title": "T-shirt",
		"merchandise_custom_form": {
			"custom_form_11": "Large",
			"custom_form_12": "Red"
		},
		"merchandise_payment_status": true,
		"merchandise_payment_amount": "$10.13"
	},
	{
		"id": "5ab0f60c5d61ab9a231800c7",
		"registration_id": 15,
		"merchandise_title": "Sweatshirt",
		"merchandise_custom_form": {
			"custom_form_11": "Medium",
			"custom_form_12": "Blue"
		},
		"merchandise_payment_status": false,
		"merchandise_payment_amount": "$60.77"
	},
	{
		"id": "5ab0f60cff31c90187a40f22",
		"registration_id": 15,
		"merchandise_title": "Sweatshirt",
		"merchandise_custom_form": {
			"custom_form_11": "Medium",
			"custom_form_12": "Blue"
		},
		"merchandise_payment_status": false,
		"merchandise_payment_amount": "$251.78"
	},
	{
		"id": "5ab0f60ceba3359ef569a4a2",
		"registration_id": 3,
		"merchandise_title": "Sweatshirt",
		"merchandise_custom_form": {
			"custom_form_11": "Medium",
			"custom_form_12": "Black"
		},
		"merchandise_payment_status": true,
		"merchandise_payment_amount": "$238.08"
	},
	{
		"id": "5ab0f60c1ef66b57f50b0daa",
		"registration_id": 11,
		"merchandise_title": "Sweatshirt",
		"merchandise_custom_form": {
			"custom_form_11": "Large",
			"custom_form_12": "Red"
		},
		"merchandise_payment_status": false,
		"merchandise_payment_amount": "$95.85"
	},
	{
		"id": "5ab0f60c48b3d8c91e15bfa0",
		"registration_id": 1,
		"merchandise_title": "Sweatshirt",
		"merchandise_custom_form": {
			"custom_form_11": "Medium",
			"custom_form_12": "Black"
		},
		"merchandise_payment_status": true,
		"merchandise_payment_amount": "$36.77"
	},
	{
		"id": "5ab0f60ce9154bdb12932239",
		"registration_id": 5,
		"merchandise_title": "Sweatshirt",
		"merchandise_custom_form": {
			"custom_form_11": "Large",
			"custom_form_12": "Blue"
		},
		"merchandise_payment_status": false,
		"merchandise_payment_amount": "$63.44"
	},
	{
		"id": "5ab0f60c28737c8995d542ac",
		"registration_id": 13,
		"merchandise_title": "Hat",
		"merchandise_custom_form": {
			"custom_form_11": "Medium",
			"custom_form_12": "Blue"
		},
		"merchandise_payment_status": true,
		"merchandise_payment_amount": "$259.71"
	},
	{
		"id": "5ab0f60c878d4ad8bf827548",
		"registration_id": 12,
		"merchandise_title": "Hat",
		"merchandise_custom_form": {
			"custom_form_11": "Medium",
			"custom_form_12": "Red"
		},
		"merchandise_payment_status": false,
		"merchandise_payment_amount": "$216.06"
	},
	{
		"id": "52b0f60c878d4ad8bf827548",
		"registration_id": 0,
		"merchandise_title": "Hat",
		"merchandise_custom_form": {
			"custom_form_11": "Medium",
			"custom_form_12": "Red"
		},
		"merchandise_payment_status": false,
		"merchandise_payment_amount": "$133.06"
	}
];

export const TOUR_RESERVATION_LIST = [
    {
        "id": "5ab2075d0caa466e15d70da8",
        "registration_id": 23,
        "tour_id": 1,
        "tour_title": "Museum Tour",
        "tour_custom_form": {
            "custom_form_13": "Wednesday",
        },
        "tour_payment_status": true,
        "tour_payment_amount": "$123.08"
    },
    {
        "id": "5ab2075df404853e80eb2e88",
        "registration_id": 9,
        "tour_id": 0,
        "tour_title": "Culture Tour",
        "tour_custom_form": { },
        "tour_payment_status": true,
        "tour_payment_amount": "$31.45"
    },
    {
        "id": "5ab2075d6347a872e1554188",
        "registration_id": 21,
        "tour_id": 2,
        "tour_title": "Paris Tour",
        "tour_custom_form": {
            "custom_form_14": "11:00"
        },
        "tour_payment_status": false,
        "tour_payment_amount": "$90.75"
    },
    {
        "id": "5ab2075d4cefd8578b48c3dd",
        "registration_id": 18,
        "tour_id": 2,
        "tour_title": "Paris Tour",
        "tour_custom_form": {
            "custom_form_14": "11:00"
        },
        "tour_payment_status": false,
        "tour_payment_amount": "$102.93"
    },
    {
        "id": "5ab2075d68375b94bd8a7148",
        "registration_id": 3,
        "tour_id": 0,
        "tour_title": "Culture Tour",
        "tour_custom_form": { },
        "tour_payment_status": false,
        "tour_payment_amount": "$285.53"
    },
    {
        "id": "5ab2075d87b60708875797de",
        "registration_id": 3,
        "tour_id": 0,
        "tour_title": "Culture Tour",
        "tour_custom_form": { },
        "tour_payment_status": true,
        "tour_payment_amount": "$121.71"
    },
    {
        "id": "5ab2075d42cb5db68d52767c",
        "registration_id": 19,
        "tour_id": 0,
        "tour_title": "Culture Tour",
        "tour_custom_form": { },
        "tour_payment_status": false,
        "tour_payment_amount": "$119.91"
    },
    {
        "id": "5ab2075d0d78413a6f208fb5",
        "registration_id": 7,
        "tour_id": 0,
        "tour_title": "Culture Tour",
        "tour_custom_form": { },
        "tour_payment_status": false,
        "tour_payment_amount": "$293.87"
    },
    {
        "id": "5ab2075ddfddee8b2fbce36e",
        "registration_id": 11,
        "tour_id": 2,
        "tour_title": "Paris Tour",
        "tour_custom_form": {
            "custom_form_14": "15:00"
        },
        "tour_payment_status": true,
        "tour_payment_amount": "$12.95"
    },
    {
        "id": "5ab2075d220f1550c6ca0899",
        "registration_id": 19,
        "tour_id": 2,
        "tour_title": "Paris Tour",
        "tour_custom_form": {
            "custom_form_14": "13:00"
        },
        "tour_payment_status": false,
        "tour_payment_amount": "$6.72"
    },
    {
        "id": "5ab2075ddc5b1cfa4e2ca211",
        "registration_id": 6,
        "tour_id": 0,
        "tour_title": "Culture Tour",
        "tour_custom_form": { },
        "tour_payment_status": false,
        "tour_payment_amount": "$180.24"
    },
    {
        "id": "5ab2075d98c3bcbe6d2bbdd5",
        "registration_id": 17,
        "tour_id": 1,
        "tour_title": "Museum Tour",
        "tour_custom_form": {
            "custom_form_13": "Wednesday",
        },
        "tour_payment_status": true,
        "tour_payment_amount": "$132.20"
    },
    {
        "id": "5ab2075dddba4a3d446ad1c5",
        "registration_id": 14,
        "tour_id": 1,
        "tour_title": "Museum Tour",
        "tour_custom_form": {
            "custom_form_13": "Wednesday",
        },
        "tour_payment_status": true,
        "tour_payment_amount": "$229.45"
    },
    {
        "id": "5ab2075d395781b4d525d6f6",
        "registration_id": 2,
        "tour_id": 1,
        "tour_title": "Museum Tour",
        "tour_custom_form": {
            "custom_form_13": "Friday",
        },
        "tour_payment_status": true,
        "tour_payment_amount": "$276.29"
    },
    {
        "id": "5ab2075dd1b26854821799eb",
        "registration_id": 23,
        "tour_id": 2,
        "tour_title": "Paris Tour",
        "tour_custom_form": {
            "custom_form_14": "11:00"
        },
        "tour_payment_status": true,
        "tour_payment_amount": "$152.28"
    }
];

export const HOTEL_RESERVATION_LIST = [
    {
        "id": "5ab20eeed3ee881e060f330b",
        "registration_id": 5,
        "hotel_id": 2,
        "hotel_title": "Gordion",
        "hotel_room_type_title": "Single",
        "hotel_custom_form": {
            "custom_form_15": true
        },
        "hotel_payment_status": false,
        "hotel_payment_amount": "$183.28",
        "hotel_check_in_date": "2017-01-06T04:38:42 -03:00",
        "hotel_check_out_date": "2014-07-22T09:39:23 -04:00"
    },
    {
        "id": "5ab20eee8a93376b1f5b7cf0",
        "registration_id": 1,
        "hotel_id": 1,
        "hotel_title": "Radisson Blu",
        "hotel_room_type_title": "Double",
        "hotel_custom_form": {
            "custom_form_16": true
        },
        "hotel_payment_status": true,
        "hotel_payment_amount": "$30.82",
        "hotel_check_in_date": "2016-07-13T04:01:49 -04:00",
        "hotel_check_out_date": "2015-06-27T12:02:01 -04:00"
    },
    {
        "id": "5ab20eeead3e7ef4b7ccd737",
        "registration_id": 24,
        "hotel_id": 1,
        "hotel_title": "Radisson Blu",
        "hotel_room_type_title": "Double",
        "hotel_custom_form": {
            "custom_form_16": true
        },
        "hotel_payment_status": true,
        "hotel_payment_amount": "$107.89",
        "hotel_check_in_date": "2015-05-18T06:22:41 -04:00",
        "hotel_check_out_date": "2015-12-04T05:11:03 -03:00"
    },
    {
        "id": "5ab20eee1bd4c178e57d4157",
        "registration_id": 9,
        "hotel_id": 2,
        "hotel_title": "Gordion",
        "hotel_room_type_title": "Single",
        "hotel_custom_form": {
            "custom_form_15": false
        },
        "hotel_payment_status": true,
        "hotel_payment_amount": "$197.85",
        "hotel_check_in_date": "2017-07-31T03:16:47 -03:00",
        "hotel_check_out_date": "2016-12-25T09:48:37 -03:00"
    },
    {
        "id": "5ab20eee2b694f593302e54a",
        "registration_id": 5,
        "hotel_id": 0,
        "hotel_title": "Gordion",
        "hotel_room_type_title": "Single",
        "hotel_custom_form": {
            "custom_form_15": false
        },
        "hotel_payment_status": false,
        "hotel_payment_amount": "$50.68",
        "hotel_check_in_date": "2017-07-09T02:54:41 -03:00",
        "hotel_check_out_date": "2014-02-13T08:23:05 -03:00"
    },
    {
        "id": "5ab20eee85f19ffa0c2b76fa",
        "registration_id": 9,
        "hotel_id": 1,
        "hotel_title": "Radisson Blu",
        "hotel_room_type_title": "Double",
        "hotel_custom_form": {
            "custom_form_16": false
        },
        "hotel_payment_status": false,
        "hotel_payment_amount": "$207.21",
        "hotel_check_in_date": "2015-03-03T02:25:10 -03:00",
        "hotel_check_out_date": "2016-10-28T01:55:54 -03:00"
    },
    {
        "id": "5ab20eeea7ab4dc45257744c",
        "registration_id": 10,
        "hotel_id": 2,
        "hotel_title": "Gordion",
        "hotel_room_type_title": "Single",
        "hotel_custom_form": {
            "custom_form_15": false
        },
        "hotel_payment_status": true,
        "hotel_payment_amount": "$10.41",
        "hotel_check_in_date": "2014-08-08T03:15:13 -04:00",
        "hotel_check_out_date": "2015-02-13T09:35:06 -03:00"
    },
    {
        "id": "5ab20eeea385054c5b488fb1",
        "registration_id": 18,
        "hotel_id": 0,
        "hotel_title": "Hilton",
        "hotel_room_type_title": "Deluxe",
        "hotel_custom_form": {},
        "hotel_payment_status": false,
        "hotel_payment_amount": "$17.12",
        "hotel_check_in_date": "2014-12-17T12:23:55 -03:00",
        "hotel_check_out_date": "2014-08-18T02:06:10 -04:00"
    },
    {
        "id": "5ab20eeef3b1b800abc3ff39",
        "registration_id": 3,
        "hotel_id": 2,
        "hotel_title": "Gordion",
        "hotel_room_type_title": "Single",
        "hotel_custom_form": {
            "custom_form_15": false
        },
        "hotel_payment_status": false,
        "hotel_payment_amount": "$56.68",
        "hotel_check_in_date": "2016-02-23T05:27:16 -03:00",
        "hotel_check_out_date": "2015-04-23T02:10:25 -04:00"
    },
    {
        "id": "5ab20eee5ee07e0e8788f95a",
        "registration_id": 6,
        "hotel_id": 0,
        "hotel_title": "Hilton",
        "hotel_room_type_title": "Single",
        "hotel_custom_form": {},
        "hotel_payment_status": true,
        "hotel_payment_amount": "$42.71",
        "hotel_check_in_date": "2017-02-14T05:35:49 -03:00",
        "hotel_check_out_date": "2015-02-15T07:34:06 -03:00"
    },
    {
        "id": "5ab20eee6765129057e3285d",
        "registration_id": 6,
        "hotel_id": 0,
        "hotel_title": "Hilton",
        "hotel_room_type_title": "Deluxe",
        "hotel_custom_form": {},
        "hotel_payment_status": true,
        "hotel_payment_amount": "$105.03",
        "hotel_check_in_date": "2017-06-23T03:12:50 -03:00",
        "hotel_check_out_date": "2014-11-06T05:33:00 -03:00"
    },
    {
        "id": "5ab20eee7c0db974b8a0f034",
        "registration_id": 8,
        "hotel_id": 2,
        "hotel_title": "Gordion",
        "hotel_room_type_title": "Single",
        "hotel_custom_form": {
            "custom_form_15": true
        },
        "hotel_payment_status": true,
        "hotel_payment_amount": "$100.54",
        "hotel_check_in_date": "2016-05-13T08:29:05 -04:00",
        "hotel_check_out_date": "2016-03-18T01:56:57 -03:00"
    },
    {
        "id": "5ab20eee928b2ea04f872835",
        "registration_id": 7,
        "hotel_id": 0,
        "hotel_title": "Hilton",
        "hotel_room_type_title": "Deluxe",
        "hotel_custom_form": {},
        "hotel_payment_status": true,
        "hotel_payment_amount": "$59.85",
        "hotel_check_in_date": "2016-02-17T05:03:29 -03:00",
        "hotel_check_out_date": "2014-02-20T03:13:31 -03:00"
    },
    {
        "id": "5ab20eeede82cf2076a620b9",
        "registration_id": 20,
        "hotel_id": 1,
        "hotel_title": "Radisson Blu",
        "hotel_room_type_title": "Double",
        "hotel_custom_form": {
            "custom_form_16": true
        },
        "hotel_payment_status": false,
        "hotel_payment_amount": "$43.85",
        "hotel_check_in_date": "2017-11-13T05:56:01 -03:00",
        "hotel_check_out_date": "2017-07-24T02:32:39 -03:00"
    },
    {
        "id": "5ab20eeefac78e0f814c742c",
        "registration_id": 20,
        "hotel_id": 2,
        "hotel_title": "Gordion",
        "hotel_room_type_title": "Single",
        "hotel_custom_form": {
            "custom_form_15": false
        },
        "hotel_payment_status": true,
        "hotel_payment_amount": "$265.15",
        "hotel_check_in_date": "2016-12-21T12:57:03 -03:00",
        "hotel_check_out_date": "2017-11-17T11:16:08 -03:00"
    }
];

export const DONATION_LIST = [
    {
        "id": "5ab21b1ab11331af5561e52e",
        "registration_id": 0,
        "donation_payment_status": false,
        "donation_payment_amount": "$160.68"
    },
    {
        "id": "5ab21b1a588a1c5229766a37",
        "registration_id": 1,
        "donation_payment_status": true,
        "donation_payment_amount": "$256.19"
    },
    {
        "id": "5ab21b1a83c33b781045a25f",
        "registration_id": 2,
        "donation_payment_status": true,
        "donation_payment_amount": "$134.24"
    },
    {
        "id": "5ab21b1a045551fe695dcb02",
        "registration_id": 3,
        "donation_payment_status": true,
        "donation_payment_amount": "$57.14"
    },
    {
        "id": "5ab21b1a8c2332d115abc690",
        "registration_id": 4,
        "donation_payment_status": false,
        "donation_payment_amount": "$190.29"
    },
    {
        "id": "5ab21b1ab8a91d0faa5eb4c7",
        "registration_id": 5,
        "donation_payment_status": false,
        "donation_payment_amount": "$79.76"
    },
    {
        "id": "5ab21b1ab32b0904df389c2a",
        "registration_id": 6,
        "donation_payment_status": false,
        "donation_payment_amount": "$131.02"
    },
    {
        "id": "5ab21b1ab46691b9f7acecf0",
        "registration_id": 7,
        "donation_payment_status": true,
        "donation_payment_amount": "$136.73"
    },
    {
        "id": "5ab21b1a961cdedd0c13e428",
        "registration_id": 8,
        "donation_payment_status": true,
        "donation_payment_amount": "$119.24"
    },
    {
        "id": "5ab21b1ac2535768e62a7790",
        "registration_id": 9,
        "donation_payment_status": false,
        "donation_payment_amount": "$46.00"
    }
];

export const PAYMENT_LIST = [
  {
    "id": "5ab4c14a8d99e8c20037545b",
    "registration_id": 0,
    "payment_status": false,
    "payment_services": [
        {
          "id": 0,
          "module_type": "spouse",
          "registration_id": 0
        },
        {
          "id": 6,
          "module_type": "spouse",
          "registration_id": 0
        },
        {
          "id": "5ab0c203223db22d6b7c5992",
          "module_type": "workshop",
          "registration_id": 0
        },
        {
          "id": "5ab21b1ab11331af5561e52e",
          "module_type": "donation",
          "registration_id": 0
        }
    ],
    "payment_amount": "$209.90",
    "payment_balance_amount": "$29.25",
    "payment_discount_amount": "$265.25",
    "payment_penalty_amount": "$230.36",
    "payment_refund_amount": "$241.54",
    "payment_idle_amount": "$154.85",
    "payment_date": "2016-10-02T04:08:02 -03:00",
    "payment_type": "cash"
  },
  {
    "id": "551b4c14a8d99e8c20037545b",
    "registration_id": 0,
    "payment_status": true,
    "payment_services": [
        {
          "id": "52b0f60c878d4ad8bf827548",
          "module_type": "merchandise",
          "registration_id": 0
        }
    ],
    "payment_amount": "$209.90",
    "payment_balance_amount": "$29.25",
    "payment_discount_amount": "$265.25",
    "payment_penalty_amount": "$230.36",
    "payment_refund_amount": "$241.54",
    "payment_idle_amount": "$154.85",
    "payment_date": "2016-10-02T04:08:02 -03:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14ac3cbc18bc143a387",
    "registration_id": 1,
    "payment_status": false,
    "payment_services": [
        {
          "id": 1,
          "module_type": "spouse",
          "registration_id": 1
        },
        {
          "id": "5ab0f60c3d794b7715b76904",
          "module_type": "merchandise",
          "registration_id": 1
        },
        {
          "id": "5ab0f60c48b3d8c91e15bfa0",
          "module_type": "merchandise",
          "registration_id": 1
        },
        {
          "id": "5ab20eee8a93376b1f5b7cf0",
          "module_type": "hotel",
          "registration_id": 1
        },
        {
          "id": "5ab21b1a588a1c5229766a37",
          "module_type": "donation",
          "registration_id": 1
        }
    ],
    "payment_amount": "$288.58",
    "payment_balance_amount": "$124.45",
    "payment_discount_amount": "$169.50",
    "payment_penalty_amount": "$217.89",
    "payment_refund_amount": "$108.85",
    "payment_idle_amount": "$79.70",
    "payment_date": "2016-09-05T02:13:41 -04:00",
    "payment_type": "credit card"
  },
  {
    "id": "5ab4c14afed2ebd27d0a7c00",
    "registration_id": 2,
    "payment_status": true,
    "payment_services": [
        {
          "id": 2,
          "module_type": "spouse",
          "registration_id": 2
        },
        {
          "id": "5ab0f60c3519497754423edc",
          "module_type": "merchandise",
          "registration_id": 2
        },
        {
          "id": "5ab2075d395781b4d525d6f6",
          "module_type": "tour",
          "registration_id": 2
        },
        {
          "id": "5ab21b1a83c33b781045a25f",
          "module_type": "donation",
          "registration_id": 2
        }
    ],
    "payment_amount": "$0.19",
    "payment_balance_amount": "$234.34",
    "payment_discount_amount": "$219.38",
    "payment_penalty_amount": "$237.58",
    "payment_refund_amount": "$18.49",
    "payment_idle_amount": "$244.85",
    "payment_date": "2015-07-22T12:42:40 -04:00",
    "payment_type": "credit card"
  },
  {
    "id": "5ab4c14a946444b4018d2697",
    "registration_id": 3,
    "payment_status": false,
    "payment_services": [
        {
          "id": 3,
          "module_type": "spouse",
          "registration_id": 3
        },
        {
          "id": "5ab0c2037dbdd4dc070c0b3e",
          "module_type": "workshop",
          "registration_id": 3
        },
        {
          "id": "5ab0f60ceba3359ef569a4a2",
          "module_type": "merchandise",
          "registration_id": 3
        },
        {
          "id": "5ab2075d68375b94bd8a7148",
          "module_type": "tour",
          "registration_id": 3
        },
        {
          "id": "5ab2075d87b60708875797de",
          "module_type": "tour",
          "registration_id": 3
        },
        {
          "id": "5ab20eeef3b1b800abc3ff39",
          "module_type": "hotel",
          "registration_id": 3
        },
        {
          "id": "5ab21b1a045551fe695dcb02",
          "module_type": "donation",
          "registration_id": 3
        }
    ],
    "payment_amount": "$223.81",
    "payment_balance_amount": "$143.67",
    "payment_discount_amount": "$155.79",
    "payment_penalty_amount": "$242.87",
    "payment_refund_amount": "$95.54",
    "payment_idle_amount": "$44.23",
    "payment_date": "2016-11-15T07:32:18 -03:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14ad3203522f8cd1f03",
    "registration_id": 4,
    "payment_status": true,
    "payment_services": [
        {
          "id": 4,
          "module_type": "spouse",
          "registration_id": 4
        },
        {
          "id": "5ab0c2035b80379d2fc3e48b",
          "module_type": "workshop",
          "registration_id": 4
        },
        {
          "id": "5ab0f60cbe62b86e546c12e5",
          "module_type": "merchandise",
          "registration_id": 4
        },
        {
          "id": "5ab21b1a8c2332d115abc690",
          "module_type": "donation",
          "registration_id": 4
        }
    ],
    "payment_amount": "$141.76",
    "payment_balance_amount": "$38.97",
    "payment_discount_amount": "$17.67",
    "payment_penalty_amount": "$60.81",
    "payment_refund_amount": "$58.32",
    "payment_idle_amount": "$297.00",
    "payment_date": "2015-04-13T10:42:24 -04:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14a2b44aa3720a4c941",
    "registration_id": 5,
    "payment_status": true,
    "payment_services": [
        {
          "id": 5,
          "module_type": "spouse",
          "registration_id": 5
        },
        {
          "id": "5ab0f60ce9154bdb12932239",
          "module_type": "merchandise",
          "registration_id": 5
        },
        {
          "id": "5ab20eeed3ee881e060f330b",
          "module_type": "hotel",
          "registration_id": 5
        },
        {
          "id": "5ab20eee2b694f593302e54a",
          "module_type": "hotel",
          "registration_id": 5
        },
        {
          "id": "5ab21b1ab8a91d0faa5eb4c7",
          "module_type": "donation",
          "registration_id": 5
        }
    ],
    "payment_amount": "$247.18",
    "payment_balance_amount": "$172.19",
    "payment_discount_amount": "$4.52",
    "payment_penalty_amount": "$129.60",
    "payment_refund_amount": "$121.66",
    "payment_idle_amount": "$94.96",
    "payment_date": "2015-10-18T06:29:53 -04:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14ac7fad7f873c260ba",
    "registration_id": 6,
    "payment_status": true,
    "payment_services": [
        {
          "id": "5ab0c2038e0b57600d9f4f75",
          "module_type": "workshop",
          "registration_id": 6
        },
        {
          "id": "5ab0f60cea372337c1e8a1ab",
          "module_type": "merchandise",
          "registration_id": 6
        },
        {
          "id": "5ab2075ddc5b1cfa4e2ca211",
          "module_type": "tour",
          "registration_id": 6
        },
        {
          "id": "5ab20eee5ee07e0e8788f95a",
          "module_type": "hotel",
          "registration_id": 6
        },
        {
          "id": "5ab20eee6765129057e3285d",
          "module_type": "hotel",
          "registration_id": 6
        },
        {
          "id": "5ab21b1ab32b0904df389c2a",
          "module_type": "donation",
          "registration_id": 6
        }
    ],
    "payment_amount": "$216.49",
    "payment_balance_amount": "$243.26",
    "payment_discount_amount": "$121.16",
    "payment_penalty_amount": "$256.52",
    "payment_refund_amount": "$288.85",
    "payment_idle_amount": "$167.20",
    "payment_date": "2015-02-15T06:01:56 -03:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14a2afec027dbc484b1",
    "registration_id": 7,
    "payment_status": false,
    "payment_services": [
        {
          "id": 9,
          "module_type": "spouse",
          "registration_id": 7
        },
        {
          "id": "5ab2075d0d78413a6f208fb5",
          "module_type": "tour",
          "registration_id": 7
        },
        {
          "id": "5ab20eee928b2ea04f872835",
          "module_type": "hotel",
          "registration_id": 7
        },
        {
          "id": "5ab21b1ab46691b9f7acecf0",
          "module_type": "donation",
          "registration_id": 7
        }
    ],
    "payment_amount": "$115.78",
    "payment_balance_amount": "$194.11",
    "payment_discount_amount": "$19.97",
    "payment_penalty_amount": "$141.24",
    "payment_refund_amount": "$36.94",
    "payment_idle_amount": "$105.41",
    "payment_date": "2016-07-31T05:34:14 -04:00",
    "payment_type": "credit card"
  },
  {
    "id": "5ab4c14a157989f1f63da4d4",
    "registration_id": 8,
    "payment_status": true,
    "payment_services": [
        {
          "id": 14,
          "module_type": "spouse",
          "registration_id": 8
        },
        {
          "id": "5ab0c20362047c09583e2531",
          "module_type": "workshop",
          "registration_id": 8
        },
        {
          "id": "5ab0c20385b1e1bae60244aa",
          "module_type": "workshop",
          "registration_id": 8
        },
        {
          "id": "5ab0c20326fa759be8c90ec9",
          "module_type": "workshop",
          "registration_id": 8
        },
        {
          "id": "5ab20eee7c0db974b8a0f034",
          "module_type": "hotel",
          "registration_id": 8
        },
        {
          "id": "5ab21b1a961cdedd0c13e428",
          "module_type": "donation",
          "registration_id": 8
        }
    ],
    "payment_amount": "$182.52",
    "payment_balance_amount": "$127.52",
    "payment_discount_amount": "$145.56",
    "payment_penalty_amount": "$287.30",
    "payment_refund_amount": "$258.93",
    "payment_idle_amount": "$142.80",
    "payment_date": "2015-08-15T02:47:36 -04:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14a788833b915be4d36",
    "registration_id": 9,
    "payment_status": true,
    "payment_services": [
        {
          "id": 13,
          "module_type": "spouse",
          "registration_id": 9
        },
        {
          "id": "5ab0c20311753aaf53b0ec97",
          "module_type": "workshop",
          "registration_id": 9
        },
        {
          "id": "5ab0c2030e44a79e8de26f51",
          "module_type": "workshop",
          "registration_id": 9
        },
        {
          "id": "5ab2075df404853e80eb2e88",
          "module_type": "tour",
          "registration_id": 9
        },
        {
          "id": "5ab20eee1bd4c178e57d4157",
          "module_type": "hotel",
          "registration_id": 9
        },
        {
          "id": "5ab20eee85f19ffa0c2b76fa",
          "module_type": "hotel",
          "registration_id": 9
        },
        {
          "id": "5ab21b1ac2535768e62a7790",
          "module_type": "donation",
          "registration_id": 9
        }
    ],
    "payment_amount": "$37.22",
    "payment_balance_amount": "$144.72",
    "payment_discount_amount": "$37.26",
    "payment_penalty_amount": "$38.76",
    "payment_refund_amount": "$273.06",
    "payment_idle_amount": "$131.11",
    "payment_date": "2014-08-09T07:09:56 -04:00",
    "payment_type": "credit card"
  },
  {
    "id": "5ab4c14a8e4819a95dd83cb1",
    "registration_id": 10,
    "payment_status": true,
    "payment_services": [
        {
          "id": "5ab0c203a405e96b800f1127",
          "module_type": "workshop",
          "registration_id": 10
        },
        {
          "id": "5ab20eeea7ab4dc45257744c",
          "module_type": "hotel",
          "registration_id": 10
        }
    ],
    "payment_amount": "$190.58",
    "payment_balance_amount": "$49.93",
    "payment_discount_amount": "$179.40",
    "payment_penalty_amount": "$216.81",
    "payment_refund_amount": "$262.71",
    "payment_idle_amount": "$22.07",
    "payment_date": "2014-01-20T03:17:30 -03:00",
    "payment_type": "credit card"
  },
  {
    "id": "5ab4c14a7ed4190362a6c29e",
    "registration_id": 11,
    "payment_status": false,
    "payment_services": [
        {
          "id": "5ab0f60c1ef66b57f50b0daa",
          "module_type": "merchandise",
          "registration_id": 11
        },
        {
          "id": "5ab2075ddfddee8b2fbce36e",
          "module_type": "tour",
          "registration_id": 11
        }
    ],
    "payment_amount": "$147.93",
    "payment_balance_amount": "$205.13",
    "payment_discount_amount": "$7.78",
    "payment_penalty_amount": "$264.39",
    "payment_refund_amount": "$141.92",
    "payment_idle_amount": "$131.84",
    "payment_date": "2017-04-28T04:01:58 -03:00",
    "payment_type": "credit card"
  },
  {
    "id": "5ab4c14a575f1903f2021e19",
    "registration_id": 12,
    "payment_status": true,
    "payment_services": [
        {
          "id": "5ab0f60c878d4ad8bf827548",
          "module_type": "merchandise",
          "registration_id": 12
        }
    ],
    "payment_amount": "$189.64",
    "payment_balance_amount": "$148.04",
    "payment_discount_amount": "$113.08",
    "payment_penalty_amount": "$214.75",
    "payment_refund_amount": "$258.21",
    "payment_idle_amount": "$279.22",
    "payment_date": "2015-08-18T07:59:40 -04:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14a78626cd9f1ac0368",
    "registration_id": 13,
    "payment_status": false,
    "payment_services": [
        {
          "id": "5ab0c2036b792ca0033e7c84",
          "module_type": "workshop",
          "registration_id": 13
        },
        {
          "id": "5ab0f60c28737c8995d542ac",
          "module_type": "merchandise",
          "registration_id": 13
        }
    ],
    "payment_amount": "$194.87",
    "payment_balance_amount": "$13.33",
    "payment_discount_amount": "$219.47",
    "payment_penalty_amount": "$260.01",
    "payment_refund_amount": "$214.69",
    "payment_idle_amount": "$50.65",
    "payment_date": "2017-11-01T01:27:46 -03:00",
    "payment_type": "credit card"
  },
  {
    "id": "5ab4c14a3d8229037fb99d95",
    "registration_id": 14,
    "payment_status": true,
    "payment_services": [
        {
          "id": "5ab0c203da0902c63e809f49",
          "module_type": "workshop",
          "registration_id": 14
        },
        {
          "id": "5ab0c203d7e7054ae958ed83",
          "module_type": "workshop",
          "registration_id": 14
        },
        {
          "id": "5ab2075dddba4a3d446ad1c5",
          "module_type": "tour",
          "registration_id": 14
        }
    ],
    "payment_amount": "$52.13",
    "payment_balance_amount": "$6.05",
    "payment_discount_amount": "$188.46",
    "payment_penalty_amount": "$256.67",
    "payment_refund_amount": "$250.63",
    "payment_idle_amount": "$221.35",
    "payment_date": "2017-05-16T12:30:06 -03:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14a803e2e4f8719c46c",
    "registration_id": 15,
    "payment_status": false,
    "payment_services": [
        {
          "id": 7,
          "module_type": "spouse",
          "registration_id": 15
        },
        {
          "id": 11,
          "module_type": "spouse",
          "registration_id": 15
        },
        {
          "id": "5ab0c2035e5910c5ae8a4e46",
          "module_type": "workshop",
          "registration_id": 15
        },
        {
          "id": "5ab0c203e3ecb0d324c8cccd",
          "module_type": "workshop",
          "registration_id": 15
        },
        {
          "id": "5ab0f60c5d61ab9a231800c7",
          "module_type": "merchandise",
          "registration_id": 15
        },
        {
          "id": "5ab0f60cff31c90187a40f22",
          "module_type": "merchandise",
          "registration_id": 15
        }
    ],
    "payment_amount": "$86.64",
    "payment_balance_amount": "$282.78",
    "payment_discount_amount": "$238.67",
    "payment_penalty_amount": "$10.95",
    "payment_refund_amount": "$58.43",
    "payment_idle_amount": "$285.26",
    "payment_date": "2014-10-24T08:48:46 -04:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14a4ffafd38f4dc3bd5",
    "registration_id": 16,
    "payment_status": true,
    "payment_services": [
        {
          "id": 15,
          "module_type": "spouse",
          "registration_id": 16
        }
    ],
    "payment_amount": "$249.34",
    "payment_balance_amount": "$245.18",
    "payment_discount_amount": "$117.15",
    "payment_penalty_amount": "$206.23",
    "payment_refund_amount": "$273.97",
    "payment_idle_amount": "$266.53",
    "payment_date": "2016-09-07T08:48:51 -03:00",
    "payment_type": "credit card"
  },
  {
    "id": "5ab4c14ab49c935bf99a865e",
    "registration_id": 17,
    "payment_status": true,
    "payment_services": [
        {
          "id": 12,
          "module_type": "spouse",
          "registration_id": 17
        },
        {
          "id": "5ab2075d98c3bcbe6d2bbdd5",
          "module_type": "tour",
          "registration_id": 17
        }
    ],
    "payment_amount": "$35.35",
    "payment_balance_amount": "$178.89",
    "payment_discount_amount": "$45.89",
    "payment_penalty_amount": "$73.62",
    "payment_refund_amount": "$229.47",
    "payment_idle_amount": "$29.26",
    "payment_date": "2015-10-13T03:58:27 -04:00",
    "payment_type": "credit card"
  },
  {
    "id": "5ab4c14ae507cc5aa18423c8",
    "registration_id": 18,
    "payment_status": false,
    "payment_services": [
        {
          "id": "5ab2075d4cefd8578b48c3dd",
          "module_type": "tour",
          "registration_id": 18
        },
        {
          "id": "5ab20eeea385054c5b488fb1",
          "module_type": "hotel",
          "registration_id": 18
        }
    ],
    "payment_amount": "$166.53",
    "payment_balance_amount": "$146.10",
    "payment_discount_amount": "$155.52",
    "payment_penalty_amount": "$209.32",
    "payment_refund_amount": "$201.15",
    "payment_idle_amount": "$50.85",
    "payment_date": "2017-06-30T09:42:13 -03:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14aa00086aed3cf7253",
    "registration_id": 19,
    "payment_status": false,
    "payment_services": [
        {
          "id": 8,
          "module_type": "spouse",
          "registration_id": 19
        },
        {
          "id": "5ab2075d42cb5db68d52767c",
          "module_type": "tour",
          "registration_id": 19
        },
        {
          "id": "5ab2075d220f1550c6ca0899",
          "module_type": "tour",
          "registration_id": 19
        }
    ],
    "payment_amount": "$82.13",
    "payment_balance_amount": "$239.95",
    "payment_discount_amount": "$174.37",
    "payment_penalty_amount": "$254.35",
    "payment_refund_amount": "$55.26",
    "payment_idle_amount": "$184.61",
    "payment_date": "2014-05-07T05:45:10 -04:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14a0c73444d11997e05",
    "registration_id": 20,
    "payment_status": false,
    "payment_services": [
        {
          "id": 10,
          "module_type": "spouse",
          "registration_id": 20
        },
        {
          "id": "5ab20eeede82cf2076a620b9",
          "module_type": "hotel",
          "registration_id": 20
        },
        {
          "id": "5ab20eeefac78e0f814c742c",
          "module_type": "hotel",
          "registration_id": 20
        }
    ],
    "payment_amount": "$42.94",
    "payment_balance_amount": "$167.91",
    "payment_discount_amount": "$139.21",
    "payment_penalty_amount": "$142.89",
    "payment_refund_amount": "$226.99",
    "payment_idle_amount": "$203.37",
    "payment_date": "2017-03-21T06:44:46 -03:00",
    "payment_type": "credit card"
  },
  {
    "id": "5ab4c14a2e883cfa7b6e675c",
    "registration_id": 21,
    "payment_status": false,
    "payment_services": [
        {
          "id": "5ab2075d6347a872e1554188",
          "module_type": "tour",
          "registration_id": 21
        }
    ],
    "payment_amount": "$153.96",
    "payment_balance_amount": "$193.22",
    "payment_discount_amount": "$39.49",
    "payment_penalty_amount": "$1.00",
    "payment_refund_amount": "$68.75",
    "payment_idle_amount": "$88.97",
    "payment_date": "2016-07-05T06:27:34 -04:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14a3688d48e6c7f99bb",
    "registration_id": 22,
    "payment_status": true,
    "payment_services": [
        {
          "id": "5ab0f60cbcec3013481652e8",
          "module_type": "merchandise",
          "registration_id": 22
        }
    ],
    "payment_amount": "$174.39",
    "payment_balance_amount": "$135.24",
    "payment_discount_amount": "$280.76",
    "payment_penalty_amount": "$253.24",
    "payment_refund_amount": "$160.24",
    "payment_idle_amount": "$197.80",
    "payment_date": "2016-10-19T05:43:01 -03:00",
    "payment_type": "credit card"
  },
  {
    "id": "5ab4c14af2fee3f93100f2cf",
    "registration_id": 23,
    "payment_status": false,
    "payment_services": [
        {
          "id": "5ab2075d0caa466e15d70da8",
          "module_type": "tour",
          "registration_id": 23
        },
        {
          "id": "5ab2075dd1b26854821799eb",
          "module_type": "tour",
          "registration_id": 23
        }
    ],
    "payment_amount": "$63.80",
    "payment_balance_amount": "$150.27",
    "payment_discount_amount": "$56.68",
    "payment_penalty_amount": "$10.29",
    "payment_refund_amount": "$123.30",
    "payment_idle_amount": "$198.99",
    "payment_date": "2014-03-22T12:55:19 -03:00",
    "payment_type": "cash"
  },
  {
    "id": "5ab4c14ad0d6b3ca1787cf9c",
    "registration_id": 24,
    "payment_status": false,
    "payment_services": [
        {
          "id": "5ab20eeead3e7ef4b7ccd737",
          "module_type": "hotel",
          "registration_id": 24
        }
    ],
    "payment_amount": "$106.61",
    "payment_balance_amount": "$17.43",
    "payment_discount_amount": "$167.65",
    "payment_penalty_amount": "$32.44",
    "payment_refund_amount": "$115.74",
    "payment_idle_amount": "$244.34",
    "payment_date": "2016-06-17T11:19:08 -04:00",
    "payment_type": "cash"
  }
];

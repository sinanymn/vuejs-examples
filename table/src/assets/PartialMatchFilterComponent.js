import Vue from "vue";
import 'lodash';

export default Vue.extend({
	template: `
		<div>
			<input type="text" v-model="search">   
			<label><input type="checkbox" v-model="select_all" @change="select_all_items">Select All</label>
			<label v-for="(item, index) in filteredItems" :for="index">
				<input type="checkbox" name="filter" v-model="text" :value="item" :id="index"> {{ item }}
			</label> 
		</div> 
	`,
	data() {
		return {
			text: [],
			valueGetter: null,
			select_all: false,
			search: '',
			item_list: []
		}
	},
	computed: {
		filteredItems() {
			return this.item_list.filter(item => {
				return String(item).toLowerCase().indexOf(this.search.toLowerCase()) > -1
			})
		}
	},
	props: ['list'],
	methods: {
		isFilterActive() {
			return this.text !== null && this.text !== undefined && this.text !== '';
		},

		doesFilterPass(params){
			return this.text.indexOf(this.valueGetter(params.node)) >= 0;
		},

		getModel() {
			return {value: this.text};
		},

		getModelAsString() {
            return {value: this.text};
        },
        destroy(){

        },
        getGui(){
        	return this.gui;
        },
        extractFilterText(){

        },
		setModel(model) {
			if(model) {
				this.text = model.value;
			}
		},
		getList(list) {
			this.item_list = _.uniqBy(list);
			this.select_all = true;
			this.select_all_items();
		},
		select_all_items(){
			if(this.select_all){
				this.text = this.item_list.map( item => item )
			}else{
				this.text = [];
			}
		}
	},
	watch: {
		'text': function(val, oldVal) {
			if (val !== oldVal) {
				this.params.filterChangedCallback(val);
			}
		}
	},
	created() {
		console.log('123')
		this.valueGetter = this.params.valueGetter;
	}
})


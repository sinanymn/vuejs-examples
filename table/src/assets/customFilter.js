import Vue from "vue";
import store from '../store/index.js';
import 'lodash';

export default Vue.extend({
	template: `
		<div>
			<button @click="showTotal">Show Total</button>	
		</div> 
	`,
	data() {
		return {
			text: [],
			valueGetter: null,
			select_all: false,
			search: '',
			item_list: []
		}
	},
	computed: {
		filteredItems() {
			return this.item_list.filter(item => {
				return String(item).toLowerCase().indexOf(this.search.toLowerCase()) > -1
			})
		}
	},
	props: ['list'],
	methods: {
		isFilterActive() {
			return this.text !== null && this.text !== undefined && this.text !== '';
		},

		doesFilterPass(params){
			return this.text.indexOf(this.valueGetter(params.node)) >= 0;
		},
		getModel() {
			return {value: this.text};
		},
		setModel(model) {
			if(model) {
				this.text = model.value;
			}
		},
		showTotal(){
			console.log('show');
			this.params.filterChangedCallback('showwww');
		}
	},
	watch: {
		'text': function(val, oldVal) {
			if (val !== oldVal) {
				this.params.filterChangedCallback(val);
			}
		}
	},
	created() {
		this.valueGetter = this.params.valueGetter;
	}
})


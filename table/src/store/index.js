import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
	filter_select_list: [],
}

const getters = {
	filter_select_list(state) {
		return state.filter_select_list;
	}
}

const mutations = {
	set_filter_select_list(state, list) {
		state.filter_select_list = list;
	},
}

const actions = {
	set_filter_select_list (context, data) {
		context.commit('set_filter_select_list', data);
	}
}

export default new Vuex.Store({
	state,
	getters,
	mutations,
	actions
});
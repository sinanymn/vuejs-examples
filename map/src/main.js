// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

// const VueGoogleMaps = require('vue2-google-maps');

// Vue.use(VueGoogleMaps, {
// 	load: {
// 		key: 'AIzaSyBp81TpMNQkydBM63AKXFG_BbJzkyGVJy8',
// 		libraries: 'places'
// 	}
// })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})

import Vue from 'vue';
export default {
	getUser(){
		return Vue.http.get('https://api.github.com/users/fatihsinanyaman')
			.then((response) => Promise.resolve(response.data))
			.catch((error) => Promise.reject(error));
	}
}
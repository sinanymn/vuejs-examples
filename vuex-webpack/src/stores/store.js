import userService from '../services/user';

const state = {
	user: {}
}

const getters = {
	user(state){
		return state.user;
	}
}

const mutations = {
	setUser (state, user) {
		state.user = user;
	}
}

const actions = {
	getUser (context){
		return userService.getUser()
		.then((response) => {
			 context.commit('setUser', response);
		})
		.catch((error) => {
			//Error handling
		})
	},
	setUser (context, user){
		return context.commit('setUser', user);
	}
}

export default {
	state,
	getters,
	mutations,
	actions
}
import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Profile from '@/components/Profile'

Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/home',
			name: 'Home',
			component: Home
		},
		{
			path: '/profile',
			name: 'Profile',
			component: Profile
		},
		{
			path: '/',
			redirect: '/home',
		}
	],
	mode: 'history',
	linkActiveClass: 'active'
})

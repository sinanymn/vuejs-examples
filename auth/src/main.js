// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import routes from './router'
import VueRouter from 'vue-router'
import VueAuth from '@websanova/vue-auth'
import VueResource from 'vue-resource';




Vue.config.productionTip = false
Vue.use(VueResource);
Vue.use(VueRouter)

Vue.http.options.root = 'https://api-demo.websanova.com/api/v1';

Vue.router = new VueRouter(routes);
const router = Vue.router;

Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/vue-resource.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    tokenDefaultName: 'MHToken',
    tokenImpersonateName: 'MHTokenImpersonate',
    tokenStore: ['localStorage', 'cookie'],
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

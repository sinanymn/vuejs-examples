import Project from '../modules/project/Project'
import ProjectSetup from '../modules/project/Setup'
import ProjectDashboard from '../modules/project/Dashboard'
import Reporting from '../modules/project/reporting/Reporting'
import Report from '../modules/project/reporting/Report'

export default [
	{
    	path: '/project/:project_id',
		name: 'Project',
		component: Project,
		redirect: { name: 'ProjectDashboard' },
		children: [
			{
				path: 'dashboard',
				name: 'ProjectDashboard',
				component: ProjectDashboard,
			},
			{
				path: 'setup',
				name: 'ProjectSetup',
				component: ProjectSetup,
			},
			{
				path: 'report',
				name: 'Reporting',
				component: Reporting,
				children: [
				{
					path: ':report_id',
					name: 'Report',
					component: Report,
				},
				]
			},
		],
	},
];

import Router from 'vue-router'
import projectRoutes from './projectRoutes.js'
import clientRoutes from './clientRoutes.js'

const routes = clientRoutes.concat(projectRoutes);

export default new Router({
	routes,
	mode: 'history'
});
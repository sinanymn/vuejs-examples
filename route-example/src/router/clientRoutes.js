import Client from '../modules/client/Client'
import ClientDashboard from '../modules/client/Dashboard'
import ProfileSettings from '../modules/client/ProfileSettings'
import Onboarding from '../modules/client/Onboarding'

export default [
	{
    	path: '/',
		name: 'Client',
		component: Client,
		redirect: { name: 'ClientDashboard' },
		children: [
			{
				path: '/dashboard',
				name: 'ClientDashboard',
				component: ClientDashboard,
			},
			{
				path: 'profile',
				name: 'ProfileSettings',
				component: ProfileSettings,
			},
			{
		    	path: 'new-project',
				name: 'Onboarding',
				component: Onboarding,
			}
		],
	},
	{
		path: '*',
		redirect: { name: 'Client' },
	},

]
